<?php
require_once './model/db.php';
if (!empty($_GET['u'])) {
    $token = htmlspecialchars(base64_decode($_GET['u']));
    $check = $bdd->prepare('SELECT * FROM sarplvup_password_recover WHERE token_user = ?');
    $check->execute(array($token));
    $row = $check->rowCount();

    if ($row == 0) {
        echo "Lien non valide";
        die();
    }
}
?>
<!doctype html>
<html lang="fr">

<head>
    <title>Réinitialiser mon mot de passe</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="./styles/style.css">
</head>

<?php include 'header.php'; ?>

<body>
    <main>
        <div class="container-full">
            <div class="wrapper-form">
                <div class="wrapper">
                    <?php
                    if (isset($_GET['recover_err'])) {
                        $err = htmlspecialchars($_GET['recover_err']);

                        switch ($err) {
                            case 'password_not_same':
                    ?>
                                <div class="alert">
                                    <p>
                                        <strong>Erreur :</strong> les mots de passes ne sont pas identiques
                                    </p>
                                </div>
                            <?php
                                break;

                            case 'void':
                            ?>
                                <div class="alert">
                                    <p>
                                        <strong>Erreur :</strong> merci de renseigner un mot de passe
                                    </p>
                                </div>
                            <?php
                                break;

                            case 'already':
                            ?>
                                <div class="alert">
                                    <p>
                                        <strong>Erreur :</strong> lien invalide
                                    </p>
                                </div>
                    <?php
                                break;
                        }
                    }
                    ?>
                    <form action="./controller/recuperation.php" class="login-form" method="POST">
                        <h1 class="text-center change-heading">Réinitialiser mon mot de passe</h1>
                        <input type="hidden" name="token" value="<?php echo base64_decode(htmlspecialchars($_GET['u'])); ?>" />
                        <div class="form__group">
                            <input type="password" name="password" class="form__field" required />
                            <label for="password" class="form__label">Mot de passe <abbr>*</abbr></label>
                            <span class="separator"> </span>
                        </div>
                        <div class="form__group">
                            <input type="password" name="password_repeat" class="form__field" required />
                            <label for="password" class="form__label">Confirmer <abbr>*</abbr></label>
                            <span class="separator"> </span>
                        </div>
                        <div class="form-group action-wrapper">
                            <button type="submit" class="btn-submit">Modifier</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </main>
</body>

</html>