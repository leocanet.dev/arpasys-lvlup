<?php 
    require_once '../model/db.php';

        if(!empty($_POST['password']) && !empty($_POST['password_repeat']) && !empty($_POST['token'])){
            $password = htmlspecialchars($_POST['password']);
            $password_repeat = htmlspecialchars($_POST['password_repeat']);
            $token = htmlspecialchars($_POST['token']);

            $check = $bdd->prepare('SELECT * FROM sarplvup_amelia_users WHERE token_user = ?');
            $check->execute(array($token));
            $row = $check->rowCount();

            if($row){
                if($password === $password_repeat){
                    $cost = ['cost' => 12];
                    $password = password_hash($password, PASSWORD_BCRYPT, $cost);

                    $update = $bdd->prepare('UPDATE sarplvup_amelia_users SET password = ? WHERE token_user = ?');
                    $update->execute(array($password, $token));
                    
                    $delete = $bdd->prepare('DELETE FROM sarplvup_password_recover WHERE token_user = ?');
                    $delete->execute(array($token));

                    header('Location: ../index.php?success=password_change');
                    die();
                }else{
                    header('Location: ../recuperation-mot-de-passe.php?recover_err=password_not_same');
                    die();
                }
            }else{
                header('Location: ../recuperation-mot-de-passe.php?recover_err=already');
                die();
            }
        }else{
            header('Location: ../recuperation-mot-de-passe.php?recover_err=void');
            die();
        }