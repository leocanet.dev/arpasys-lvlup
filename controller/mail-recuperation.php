<?php 
    require_once '../model/db.php';

   if(!empty($_GET['u']) && !empty($_GET['token']) ){
       
    $u = htmlspecialchars(base64_decode($_GET['u']));
    $token = htmlspecialchars(base64_decode($_GET['token']));
       
    $check = $bdd->prepare('SELECT * FROM sarplvup_password_recover WHERE token_user = ? AND token = ?');
    $check->execute(array($u, $token));
    $row = $check->rowCount();
    $data = $check->fetch();
       
    if($row){
        
        $get = $bdd->prepare('SELECT token_user FROM sarplvup_amelia_users WHERE token_user = ?');
        $get->execute(array($u));
        $data_u = $get->fetch();
        
        if(hash_equals($data_u['token_user'], $u)){
            header('Location: ../recuperation-mot-de-passe.php?u='.base64_encode($u));
            die();
        }else{
            header('Location: ../mot-de-passe-oublie.php?recover_err=token');
            die();
        }
    }else{
        header('Location: ../mot-de-passe-oublie.php?recover_err=link');
        die();
    }
}else {
    header('Location: ../mot-de-passe-oublie.php?recover_err=link');
    die();
}