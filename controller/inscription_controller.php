<?php
require_once '../model/db.php'; // On inclu la connexion à la bdd
include '../debug.php';

// Si les variables existent et qu'elles ne sont pas vides
if (!empty($_POST['prenom']) && !empty($_POST['nom']) && !empty($_POST['naissance']) && !empty($_POST['email']) && !empty($_POST['phone']) && !empty($_POST['password']) && !empty($_POST['password_retype']) && !empty($_POST['naissance']) && !empty($_POST['rue']) && !empty($_POST['zipcode']) && !empty($_POST['city'])) {
    // Patch XSS
    $prenom = htmlspecialchars($_POST['prenom']);
    $nom = htmlspecialchars($_POST['nom']);
    $date = htmlspecialchars($_POST['naissance']);
    $email = htmlspecialchars($_POST['email']);
    $phone = htmlspecialchars($_POST['phone']);
    $password = htmlspecialchars($_POST['password']);
    $password_retype = htmlspecialchars($_POST['password_retype']);
    $rue = htmlspecialchars($_POST['rue']);
    $zipcode = htmlspecialchars($_POST['zipcode']);
    $city = htmlspecialchars($_POST['city']);
    $societe = htmlspecialchars($_POST['societe']);
    $tva = htmlspecialchars($_POST['tva']);

    // On vérifie si l'utilisateur existe
    $check = $bdd->prepare('SELECT firstName, lastName, email FROM sarplvup_amelia_users WHERE email = ?');
    $check->execute(array($email));
    $data = $check->fetch();
    $row = $check->rowCount();

    $email = strtolower($email); // on transforme toute les lettres majuscule en minuscule pour éviter que Foo@gmail.com et foo@gmail.com soient deux compte différents ..

    // Si la requete renvoie un 0 alors l'utilisateur n'existe pas 
    if ($row == 0) {
        if (strlen($prenom) <= 40) { // On verifie que la longueur du prenom <= 40
            if (strlen($nom) <= 40) {
                if (strlen($email) <= 40) {
                    if (filter_var($email, FILTER_VALIDATE_EMAIL)) { // Si l'email est de la bonne forme
                        if (strlen($rue) <= 30) {
                            if (strlen($zipcode) == 5) {
                                if (strlen($city) <= 30) {
                                    if ($password === $password_retype) { // si les deux mdp saisis sont bon

                                        // On hash le mot de passe avec Bcrypt, via un coût de 12
                                        $cost = ['cost' => 12];
                                        $password = password_hash($password, PASSWORD_BCRYPT, $cost);

                                        // On insère dans la base de données
                                        $insert = $bdd->prepare("INSERT INTO sarplvup_amelia_users (firstName, lastName, birthday, email, phone, password, token_user, type, status, rue, zipcode, city, societe, tva) VALUES (:firstName, :lastName, :birthday, :email, :phone, :password, :token_user, 'customer', 'visible', :rue, :zipcode, :city, :societe, :tva)");
                                        $insert->execute(array(
                                            'firstName' => $prenom,
                                            'lastName' => $nom,
                                            'birthday' => $date,
                                            'email' => $email,
                                            'phone' => $phone,
                                            'password' => $password,
                                            'token_user' => bin2hex(openssl_random_pseudo_bytes(64)),
                                            'rue' => $rue,
                                            'zipcode' => $zipcode,
                                            'city' => $city,
                                            'societe' => $societe,
                                            'tva' => $tva,

                                        ));
                                        // On redirige avec le message de succès
                                        header('Location: ../index.php?success=success');
                                        die();
                                    } else {
                                        header('Location: ../inscription.php?reg_err=password');
                                        die();
                                    }
                                } else {
                                    header('Location: ../inscription.php?reg_err=city');
                                    die();
                                }
                            } else {
                                header('Location: ../inscription.php?reg_err=zipcode');
                                die();
                            }
                        } else {
                            header('Location: ../inscription.php?reg_err=rue');
                            die();
                        }
                    } else {
                        header('Location: ../inscription.php?reg_err=email');
                        die();
                    }
                } else {
                    header('Location: ../inscription.php?reg_err=email_length');
                    die();
                }
            } else {
                header('Location: ../inscription.php?reg_err=nom_length');
                die();
            }
        } else {
            header('Location: ../inscription.php?reg_err=prenom_length');
            die();
        }
    } else {
        header('Location: ../inscription.php?reg_err=already');
        die();
    }
}
