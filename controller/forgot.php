<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require '../vendor/phpmailer/phpmailer/src/PHPMailer.php';
require '../vendor/phpmailer/phpmailer/src/SMTP.php';
require '../vendor/phpmailer/phpmailer/src/Exception.php';
require '../vendor/autoload.php';
require_once '../model/db.php';

if (!empty($_POST['email'])) {
    $email = htmlspecialchars($_POST['email']);
    $check = $bdd->prepare('SELECT token_user FROM sarplvup_amelia_users WHERE email = ?');
    $check->execute(array($email));
    $data = $check->fetch();
    $row = $check->rowCount();

    if ($row) {
        $token = bin2hex(openssl_random_pseudo_bytes(24));
        $token_user = $data['token_user'];
        if($token_user === NULL){
            $token_user = bin2hex(openssl_random_pseudo_bytes(64));
            $update = $bdd->prepare('UPDATE sarplvup_amelia_users SET `token_user`=:newToken WHERE `email`=:email ');
            $update->execute(array(
                'newToken' => $token_user,
                'email' => $email,
            ));
        }
        $insert = $bdd->prepare('INSERT INTO sarplvup_password_recover (token_user, token) VALUES(?,?)');
        $insert->execute(array($token_user, $token));
        $link = 'https://test.arpaview.fr/SITE-ARPASYS-LVLUP/espace-client/controller/mail-recuperation.php?u=' . base64_encode($token_user) . '&token=' . base64_encode($token);
        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = SMTP::DEBUG_SERVER;
            $mail->isSMTP();
            $mail->Host       = 'ssl0.ovh.net';
            $mail->SMTPAuth   = true;
            $mail->Username   = 'support@leocanet.fr';
            $mail->Password   = '41y3rqJ%CCe1';
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->Port       = 587;

            //From & to 
            $mail->setFrom('support@leocanet.fr', 'Arpasys');
            $mail->addAddress($email);

            //Content
            $mail->isHTML(true);
            $mail->Subject = 'Recuperation de mot de passe';
            $mail->Body    = "Nous vous informons qu'une demande de changement de mot de passe a été enregistrée sur notre site pour les comptes associés à l'adresse e-mail : <b>$email</b>
            <br>
            <br>
            Si vous êtes à l'origine de cette demande et que vous souhaitez toujours remplacer votre mot de passe,
            <br>
            il vous suffit de cliquer sur le lien ci-dessous, ou de le recopier dans votre navigateur.
            <br>
            <br>
             <b>$link</b>";

            $mail->send();
            header('Location: ../index.php?success=recover');
            die();
        } catch (Exception $e) {
            echo "L'email n'a pas pu être envoyé Erreur: {$mail->ErrorInfo}";
        }
    } else {
        header('Location: ../mot-de-passe-oublie.php?recover_err=already');
        die();
    }
}
