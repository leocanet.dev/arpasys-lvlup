<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./styles/style.css">
    <title>Mot de passe oublié</title>
</head>

<?php include 'header.php'; ?>

<body>
    <main>
        <div class="container-full">
            <div class="wrapper-form">
                <div class="wrapper">
                    <?php
                    if (isset($_GET['recover_err'])) {
                        $err = htmlspecialchars($_GET['recover_err']);

                        switch ($err) {
                            case 'token':
                    ?>
                                <div class="alert">
                                    <p>
                                        <strong>Erreur :</strong> token non valide
                                    </p>
                                </div>
                            <?php
                                break;

                            case 'link':
                            ?>
                                <div class="alert">
                                    <p>
                                        <strong>Erreur :</strong> lien expiré
                                    </p>
                                </div>
                    <?php
                                break;
                        }
                    }
                    ?>
                    <div class="form_group">
                        <form action="./controller/forgot.php" class="login-form" method="post">
                            <h1 class="text-center change-heading">Récupération de compte</h1>
                            <p class="change-text">Saisissez votre adresse e-mail et vous recevrez un lien de réinitialisation de votre mot de passe.</p>
                            <div class="form__group">
                                <input type="email" class="form__field" name="email" placeholder="email" autocomplete="off" required />
                                <label for="email" class="form__label">Email</label>
                            </div>
                            <span class="separator"> </span>
                            <div class="form-group action-wrapper">
                                <button type="submit" class="btn-submit">Envoyer</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</body>

</html>