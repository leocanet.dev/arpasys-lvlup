<?php
session_start();
require_once './model/data.php';
require_once './model/db.php';
date_default_timezone_set('UTC+2');

// si la session n'existe pas ou si l'user n'est pas connecté on redirige
if (!isset($_SESSION['user'])) {
    header('Location:index.php');
    die();
}
// récupere les données de l'utilisateur
$req = $bdd->prepare('SELECT * FROM sarplvup_amelia_users WHERE token_user = ?');
$req->execute(array($_SESSION['user']));
$data = $req->fetch();
// stocke l'id du client
$customerId = $data['id'];
// récupère les données de l'utilisateur connecté
$customerDatas = getUserDatas($customerId);
// recupère les events à venir de l'utilisateur
$customerAvailableEvents = getUserAvailableEvents($customerId);
// recupère les events passés de l'utilisateur
$customerPastEvents = getUserPastEvents($customerId);
// update user datas
if ($_POST) {
    if (
        !empty($_POST['phone'])
        && !empty($_POST['birthday'])
        && !empty($_POST['rue'])
        && !empty($_POST['zipcode'])
        && !empty($_POST['city'])
    ) {
        // On nettoie les données envoyées
        $phone = strip_tags($_POST['phone']);
        $birthday = strip_tags($_POST['birthday']);
        $rue = strip_tags($_POST['rue']);
        $zipcode = strip_tags($_POST['zipcode']);
        $city = strip_tags($_POST['city']);
        $societe = strip_tags($_POST['societe']);
        $tva = strip_tags($_POST['tva']);

        $sql = 'UPDATE `sarplvup_amelia_users` SET `phone`=:phone, `birthday`=:birthday, `rue`=:rue, `zipcode`=:zipcode, `city`=:city, `societe`=:societe, `tva`=:tva WHERE `id`=:id;';
        $query = $bdd->prepare($sql);
        $query->bindValue(':id', $customerId, PDO::PARAM_INT);
        $query->bindValue(':phone', $phone, PDO::PARAM_STR);
        $query->bindValue(':birthday', $birthday, PDO::PARAM_STR);
        $query->bindValue(':rue', $rue, PDO::PARAM_STR);
        $query->bindValue(':zipcode', $zipcode, PDO::PARAM_INT);
        $query->bindValue(':city', $city, PDO::PARAM_STR);
        $query->bindValue(':societe', $societe, PDO::PARAM_STR);
        $query->bindValue(':tva', $tva, PDO::PARAM_STR);
        $query->execute();
        // alerte l'utilisateur du succés de la modification et le redirige
        header('Location: landing.php?err=success_change');
        die();
    }
}
?>
<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./styles/style.css">
    <title>Espace client</title>
</head>

<?php include 'header.php'; ?>

<body>
    <div class="full-container">
        <div class="info-wrapper">
            <h1 class="text-center">Espace client</h1>
            <div class="minwrap">
                <div class="user-wrapper">
                    <div id="landing-modal" class="modal">

                        <div class="modal-content">
                            <span class="close">&times;</span>
                            <div class="update">
                                <h2 class="text-center">Modifier vos informations</h2>
                                <form class="login-form" method="post">
                                    <div class="box-wrapper update-wrapper">
                                        <div class="update-form">
                                            <div class="form__group">
                                                <input type="tel" name="phone" value="<?= $data['phone'] ?>" class="form__field" required="required" autocomplete="off" min="10" max="10" pattern="[0-9]{10}">
                                                <label for="phone" class="form__label">Téléphone <abbr>*</abbr></label>

                                            </div>
                                            <div class="form__group">
                                                <input type="date" name="birthday" value="<?= $data['birthday'] ?>" class="form__field" required="required" autocomplete="off">
                                                <label for="birthday" class="form__label">Date de naissance <abbr>*</abbr></label>

                                            </div>
                                            <div class="form__group">
                                                <input type="text" name="rue" value="<?= $data['rue'] ?>" class="form__field" required="required" autocomplete="off">
                                                <label for="rue" class="form__label">Rue <abbr>*</abbr></label>

                                            </div>
                                            <div class="form__group">
                                                <input type="tel" name="zipcode" value="<?= $data['zipcode'] ?>" class="form__field" required="required" autocomplete="off" min="4" max="5" pattern="[0-9]{4,5}">
                                                <label for="zipcode" class="form__label">Code Postal <abbr>*</abbr></label>

                                            </div>
                                            <div class="form__group">
                                                <input type="text" name="city" value="<?= $data['city'] ?>" class="form__field" required="required" autocomplete="off">
                                                <label for="city" class="form__label">Ville <abbr>*</abbr></label>

                                            </div>
                                        </div>
                                        <div class="update-entreprise">
                                            <h3 class="text-center">Vous faîtes partie d'une société ?</h3>
                                            <div class="form__group">
                                                <input type="text" name="societe" value="<?= $data['societe'] ?>" class="form__field" autocomplete="off">
                                                <label for="societe" class="form__label">Nom de la société</label>

                                            </div>
                                            <div class="form__group">
                                                <input type="text" name="tva" value="<?= $data['tva'] ?>" class="form__field" autocomplete="off" min="10" max="15" pattern="[0-9]{10,15}">
                                                <label for="tva" class="form__label">N° TVA</label>
                                            </div>
                                            <p style="margin-top: 2rem;">Les champs avec <abbr>*</abbr> sont obligatoires.</p>
                                        </div>
                                    </div>
                            </div>
                            <div class="form-group update-action-wrapper">
                                <button type="submit" class="btn-submit">Modifier mes informations</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php
                // Je boucle sur customerEventData pour afficher les données
                foreach ($customerDatas as $data) {
                ?>
                    <div class="info-box-wrapper">
                        <div class="info-box-1">
                            <div class="user-label"><b>Prenom : </b><?= $data["firstName"] ?></div>
                            <div class="user-label"><b>Nom : </b><?= $data["lastName"] ?></div>
                            <div class="user-label cache"><b>Date de naissance : </b>

                                <?php if ($data["birthday"] == NULL) { ?>
                                    <em style="color: gray;">À renseigner *</em>
                                <?php } else { ?>
                                    <?= date('d/m/Y', strtotime($data['birthday'])) ?>
                                <?php } ?>
                            </div>
                            <div class="user-label"><b>Email : </b>

                                <?= $data["email"] ?>
                            </div>
                            <div class="user-label"><b>Téléphone : </b>

                                <?php if ($data['phone'] == NULL) { ?>
                                    <em style="color: gray;">À renseigner *</em>
                                <?php } else { ?>
                                    <?= $data["phone"] ?>
                                <?php } ?>
                            </div>
                            <div class="user-label cache"><b>Adresse : </b>

                                <?php if ($data['rue'] == NULL) { ?>
                                    <em style="color: gray;">À renseigner *</em>
                                <?php } else { ?>
                                    <?= $data["rue"] ?>
                                <?php } ?>
                            </div>
                            <div class="user-label cache"><b>Code postal : </b>

                                <?php if ($data['zipcode'] == NULL) { ?>
                                    <em style="color: gray;">À renseigner *</em>
                                <?php } else { ?>
                                    <?= $data["zipcode"] ?>
                                <?php } ?>
                            </div>
                            <p class="user-label cache"><b>Ville : </b>

                                <?php if ($data['city'] == NULL) { ?>
                                    <em style="color: gray;">À renseigner *</em>
                                <?php } else { ?>
                                    <?= $data["city"] ?>
                                <?php } ?>
                        </div>
                        <hr class="mb">
                        <div class="user-label"><b>Société : </b>

                            <?php if ($data['societe'] == NULL) { ?>
                                <em style="color: gray;">Vous n'êtes pas assigné à une société</em>
                            <?php } else { ?>
                                <?= $data["societe"] ?>
                            <?php } ?>
                        </div>
                        <div class="user-label"><b>Numero TVA : </b>

                            <?php if ($data['tva'] == NULL || $data['tva'] == 0) { ?>
                                <em style="color: gray;">Vous n'avez pas de n° TVA</em>
                            <?php } else { ?>
                                <?= $data["tva"] ?>
                            <?php } ?>
                        </div>
                        <?php if ($data['birthday'] == NULL || $data['rue'] == NULL || $data['zipcode'] == NULL || $data['city'] == NULL) { ?>
                                    <hr>
                                    <p class="no-info">Veuillez renseigner votre <b>date de naissance</b> ainsi que votre <b>adresse postale</b></p>
                                <?php } ?>
                    </div>
                    <div class="box-btn-change">
                        <div class="box-dual-btn">
                            <button class="btn-submit action-btn" style="width: 162px;" id="landing-btn">Modifier</button>
                            <button class="btn-submit action-btn" style="width: 162px; margin-left: 10px;" id="password-btn">Mot de passe</button>
                        </div>
                        <div class="box-dual-btn">
                            <button class="btn-submit" style="width: 162px;" id="tabPasse">Historique</button>
                            <a class='btn-submit action-btn' style=" width: 132px; ;margin-left: 10px;" id='codeco' title='Déconnexion' href='./controller/deconnexion.php'>Déconnexion</a>
                        </div>
                    </div>

                <?php } ?>
            </div>
        </div>
        <div class="reset-wrapper">
            <div class="reset-dialog">
                <div class="wrapper change">
                    <div id="password-modal" class="modal">

                        <div class="modal-content">
                            <span class="close">&times;</span>
                            <h2 class="text-center">Changer votre mot de passe</h2>
                            <div class="wrapper update">
                                <form action="./controller/change_password.php" method="post">
                                    <div class="form__group">
                                        <input type="password" class="form__field" name="current_password" required />
                                        <label for="current_password" class="form__label">Mot de passe actuel <abbr>*</abbr></label>
                                        <span class="separator"> </span>
                                    </div>
                                    <div class="form__group">
                                        <input type="password" class="form__field" name="new_password" required />
                                        <label for="new_password" class="form__label">Nouveau mot de passe <abbr>*</abbr></label>
                                        <span class="separator"> </span>
                                    </div>
                                    <div class="form__group">
                                        <input type="password" class="form__field" name="new_password_retype" required />
                                        <label for="new_password_retype" class="form__label">Re tapez le nouveau mot de passe <abbr>*</abbr></label>
                                        <span class="separator"> </span>
                                    </div>
                                    <div class="form-group update-action-wrapper">
                                        <button type="submit" class="btn-submit">Sauvegarder</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="customer-wrapper">
            <?php
            if (isset($_GET['err'])) {
                $err = htmlspecialchars($_GET['err']);
                switch ($err) {
                    case 'current_password':
            ?>
                        <div class="alert alert-landing">
                            <p>
                                <strong>Erreur :</strong> mot de passe incorrect
                            </p>
                        </div>
                    <?php
                        break;

                    case 'success_password':
                    ?>
                        <div class="alert alert-ok-landing">
                            <p>
                                <strong>Succès :</strong> le mot de passe a bien été modifié
                            </p>
                        </div>
                    <?php

                        break;
                    case 'success_change':
                    ?>
                        <div class="alert alert-ok-landing">
                            <p>
                                <strong>Succès :</strong> vos informations ont bien été modifiées
                            </p>
                        </div>
            <?php

                        break;
                }
            }
            ?>

            <div class="list-wrapper ">
                <div class="mintab">
                    <h2 class="text-center">Vos prochains cours</h2>

                    <?php if (count($customerAvailableEvents) == 0) { ?>
                        <p class="text-content">Vous n'avez pas encore réservé de cours</p>
                        <p class="text-content">Réservez votre premier cours en cliquant sur ce <a href="https://test.arpaview.fr/SITE-ARPASYS-LVLUP/" style="color: #7350f2!important; font-weight: bold;">lien</a> </p>
                        <?php } else {

                        for ($j = 0; $j < count($customerAvailableEvents); $j++) {
                            $dateDebut = date("d/m/Y H:i", strtotime($customerAvailableEvents[$j]['periodStart'] . "+2hours"));
                            $dateFin = date("H:i", strtotime($customerAvailableEvents[$j]['periodEnd'] . "+2hours"));
                        ?>
                            <div class="coursListe">

                                <h3><?= $customerAvailableEvents[$j]["name"] ?></h3>
                                <div class="sep">
                                    <div class="gauche">
                                        <p><b><?= $dateDebut ?>h-<?= $dateFin ?>h</b> avec <?= $customerAvailableEvents[$j]["firstName"] ?> <?= $customerAvailableEvents[$j]["lastName"] ?></p>
                                        <p><?= $customerAvailableEvents[$j]["persons"] ?> personne<?php if ($customerAvailableEvents[$j]["persons"] > 1) { ?>s (vous inclus)<?php } ?></p>
                                        <p><b>Votre cours est <?php if ($customerAvailableEvents[$j]["status"] == "approved") {
                                                                    echo ' toujours prévu';
                                                                } else {

                                                                    echo 'annulé';
                                                                } ?> </b></p>

                                    </div>
                                    <div class="droite">
                                        <p><?= $customerAvailableEvents[$j]["description"] ?></p>
                                    </div>
                                </div>
                                <hr>
                            </div>
                        <?php } ?>
                    <?php } ?>

                    <div id="event-passe-modal" class="modal">
                        <div class="modal-content">
                            <span class="close">&times;</span>
                            <h2 class="text-center">Vos cours précédents</h2>
                            <div class="table-wrapper modal-tab" id='secretbutton'>
                                <?php if (count($customerPastEvents) == 0) { ?>
                                    <p class="text-content">Vous n'avez pas d'anciens cours</p><br>
                                    <p class="text-content">Vous pouvez en reserver un en cliquant sur ce <a href="https://test.arpaview.fr/SITE-ARPASYS-LVLUP/" style="color: #7350f2!important;">lien</a> </p>
                                <?php } else { ?>
                                    <div class="table-wrapper-data">
                                        <div id="paginated-list" data-current-page="1" aria-live="polite">
                                            <table class="table">
                                                <thead>
                                                    <tr style="display: contents!important;">
                                                        <th class="cache">Prix</th>
                                                        <th class="cache">&#215;</th>
                                                        <th>Statut</th>
                                                        <th>Date</th>
                                                        <th>Nom</th>
                                                        <th class="cache">Description</th>
                                                        <th>Formateur</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php for ($k = 0; $k < count($customerPastEvents); $k++) {
                                                        $dateDebut = date("d/m/Y H:i", strtotime($customerPastEvents[$k]['periodStart'] . "+2hours"));
                                                        $dateFin = date("H:i", strtotime($customerPastEvents[$k]['periodEnd'] . "+2hours")); ?>
                                                        <tr class="t-row">
                                                            <td class="no-wrapp cache"><?= $customerPastEvents[$k]["price"] ?>€</td>
                                                            <td class="no-wrapp cache">x<?= $customerPastEvents[$k]["persons"] ?></td>
                                                            <td class="no-wrapp" title="<?php if ($customerPastEvents[$k]["status"] == "approved") {
                                                                                            echo 'Approuvé';
                                                                                        } else {
                                                                                            echo 'Annulé';
                                                                                        } ?>"><?php if ($customerPastEvents[$k]["status"] == "approved") {
                                                                                                    echo '<svg class="checked" fill="#4FA720" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!-- Font Awesome Pro 5.15.4 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) --><path d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z"/></svg>';
                                                                                                } else {
                                                                                                    echo '<svg class="checked" fill="#BF2C2C" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!-- Font Awesome Pro 5.15.4 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) --><path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z"/></svg>';
                                                                                                } ?></td>
                                                            <td class="no-wrapp event-date"><?= $dateDebut ?>h-<?= $dateFin ?>h</td>
                                                            <td class="no-wrapp nom"><?= $customerPastEvents[$k]["name"] ?></td>
                                                            <td class="event-text event-descr cache no-wrapp"><?= $customerPastEvents[$k]["description"] ?></td>
                                                            <td class="event-text no-wrapp"><?= $customerPastEvents[$k]["firstName"] ?> <?= $customerPastEvents[$k]["lastName"] ?></td>
                                                        </tr>
                                                    <?php } ?>

                                        </div>
                                        </tbody>
                                        </table>
                                    </div>
                                    <nav class="pagination-container">
                                        <button class="pagination-button" id="prev-button" aria-label="Previous page" title="Previous page">
                                            &lt;
                                        </button>

                                        <div id="pagination-numbers">

                                        </div>

                                        <button class="pagination-button" id="next-button" aria-label="Next page" title="Next page">
                                            &gt;
                                        </button>
                                    </nav>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="modal-list">

                            <?php if (count($customerPastEvents) == 0) { ?>
                                <p class="text-content">Vous n'avez pas d'anciens cours</p>
                                <p class="text-content">Vous pouvez en reserver un en cliquant sur ce <a href="https://test.arpaview.fr/SITE-ARPASYS-LVLUP/" style="color: #7350f2!important;">lien</a> </p>
                            <?php } else { ?>
                                <div id="paginated-list-list" data-current-page="1" aria-live="polite">
                                    <?php for ($k = 0; $k < count($customerPastEvents); $k++) {
                                        $dateDebut = date("d/m/Y H:i", strtotime($customerPastEvents[$k]['periodStart'] . "+2hours"));
                                        $dateFin = date("H:i", strtotime($customerPastEvents[$k]['periodEnd'] . "+2hours")); ?>

                                        <div class="page-list">
                                            <div class='wrapper-list'>
                                                <p class="no-wrapp nom"><?= $customerPastEvents[$k]["name"] ?></p>
                                            </div>
                                            <div class='wrapper-list-descr'>
                                                <p class="no-wrapp event-date"><?= $dateDebut ?>h-<?= $dateFin ?>h</p>
                                                <p class="event-text no-wrapp"><?= $customerPastEvents[$k]["firstName"] ?> <?= $customerPastEvents[$k]["lastName"] ?></p>
                                            </div>
                                            <p class="no-wrapp" style="margin: 0px; text-align:center;" title="<?php if ($customerPastEvents[$k]["status"] == "approved") {
                                                                                                                    echo 'Approuvé';
                                                                                                                } else {
                                                                                                                    echo 'Annulé';
                                                                                                                } ?>"><?php if ($customerPastEvents[$k]["status"] == "approved") {
                                                                                        echo '<svg class="checked" fill="#4FA720" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!-- Font Awesome Pro 5.15.4 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) --><path d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z"/></svg>';
                                                                                    } else {
                                                                                        echo '<svg class="checked" fill="#BF2C2C" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!-- Font Awesome Pro 5.15.4 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) --><path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm121.6 313.1c4.7 4.7 4.7 12.3 0 17L338 377.6c-4.7 4.7-12.3 4.7-17 0L256 312l-65.1 65.6c-4.7 4.7-12.3 4.7-17 0L134.4 338c-4.7-4.7-4.7-12.3 0-17l65.6-65-65.6-65.1c-4.7-4.7-4.7-12.3 0-17l39.6-39.6c4.7-4.7 12.3-4.7 17 0l65 65.7 65.1-65.6c4.7-4.7 12.3-4.7 17 0l39.6 39.6c4.7 4.7 4.7 12.3 0 17L312 256l65.6 65.1z"/></svg>';
                                                                                    } ?></p>
                                            <hr>
                                        </div>
                                    <?php } ?>
                                </div>
                                <nav class="pagination-container">
                                    <button class="pagination-button" id="prev-button-list" aria-label="Previous page" title="Previous page">
                                        &lt;
                                    </button>

                                    <div id="pagination-numbers-list">

                                    </div>

                                    <button class="pagination-button" id="next-button-list" aria-label="Next page" title="Next page">
                                        &gt;
                                    </button>
                                </nav>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script src="./assets/landing.js"></script>
</body>

</html>