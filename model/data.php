<?php

require_once 'db.php';
include '../debug.php';

function getUserDatas($customerId)
{
    global $bdd;
    $req = $bdd->prepare('SELECT firstName, lastName, email, birthday, phone, rue, zipcode, city, societe, tva FROM `sarplvup_amelia_users` WHERE id = ?');
    $req->execute(array($customerId));
    return $req->fetchAll(PDO::FETCH_ASSOC);
};

function getUserAvailableEvents($customerId)
{
    $tableauPresent = array();
    $date = new DateTime;
    $date = date_format($date, "Y-m-d H:i:s");
    global $bdd;
    // récupère les données des events associées à l'utilisateur connecté, les plus récents en premier
    $reqTabPres = $bdd->prepare('SELECT sarplvup_amelia_customer_bookings.price, sarplvup_amelia_customer_bookings.persons, sarplvup_amelia_events.status, sarplvup_amelia_events_periods.periodStart, sarplvup_amelia_events_periods.periodEnd, sarplvup_amelia_events.name, sarplvup_amelia_events.description, sarplvup_amelia_users.firstName, sarplvup_amelia_users.lastName, sarplvup_amelia_customer_bookings_to_events_tickets.persons FROM `sarplvup_amelia_customer_bookings` 
    INNER JOIN `sarplvup_amelia_customer_bookings_to_events_periods` ON sarplvup_amelia_customer_bookings.id = sarplvup_amelia_customer_bookings_to_events_periods.customerBookingId 
    INNER JOIN `sarplvup_amelia_events` ON sarplvup_amelia_customer_bookings_to_events_periods.eventPeriodId = sarplvup_amelia_events.id
    INNER JOIN `sarplvup_amelia_customer_bookings_to_events_tickets` ON sarplvup_amelia_customer_bookings_to_events_tickets.customerBookingId = sarplvup_amelia_customer_bookings.id
    INNER JOIN `sarplvup_amelia_events_periods` ON sarplvup_amelia_events.id = sarplvup_amelia_events_periods.id 
    INNER JOIN `sarplvup_amelia_events_to_providers` ON sarplvup_amelia_events_periods.id = sarplvup_amelia_events_to_providers.eventId 
    INNER JOIN `sarplvup_amelia_users` ON sarplvup_amelia_events_to_providers.userId = sarplvup_amelia_users.id 
    WHERE sarplvup_amelia_customer_bookings.customerId = :customerId ORDER BY sarplvup_amelia_events_periods.periodStart ASC');

    $reqTabPres->bindValue(':customerId', $customerId, PDO::PARAM_INT);
    $reqTabPres->execute();
    $tabPres = $reqTabPres->fetchAll(PDO::FETCH_ASSOC);

    // trie les cours dans tableauPresent en fonction de la date de l'event et la date du jour
    for ($u = 0; $u < count($tabPres); $u++) {

        if ($tabPres[$u]['periodStart'] > $date) {
            array_push($tableauPresent, $tabPres[$u]);
        }
    }
    return $tableauPresent;
};

function getUserPastEvents($customerId)
{
    $tableauPasse = array();
    $date = new DateTime;
    $date = date_format($date, "Y-m-d H:i:s");
    global $bdd;
    // récupère les données des events associées à l'utilisateur connecté, les plus récents en premier
    $reqTabPres = $bdd->prepare('SELECT sarplvup_amelia_customer_bookings.price, sarplvup_amelia_customer_bookings.persons, sarplvup_amelia_events.status, sarplvup_amelia_events_periods.periodStart, sarplvup_amelia_events_periods.periodEnd, sarplvup_amelia_events.name, sarplvup_amelia_events.description, sarplvup_amelia_users.firstName, sarplvup_amelia_users.lastName, sarplvup_amelia_customer_bookings_to_events_tickets.persons FROM `sarplvup_amelia_customer_bookings` 
    INNER JOIN `sarplvup_amelia_customer_bookings_to_events_periods` ON sarplvup_amelia_customer_bookings.id = sarplvup_amelia_customer_bookings_to_events_periods.customerBookingId 
    INNER JOIN `sarplvup_amelia_events` ON sarplvup_amelia_customer_bookings_to_events_periods.eventPeriodId = sarplvup_amelia_events.id
    INNER JOIN `sarplvup_amelia_customer_bookings_to_events_tickets` ON sarplvup_amelia_customer_bookings_to_events_tickets.customerBookingId = sarplvup_amelia_customer_bookings.id
    INNER JOIN `sarplvup_amelia_events_periods` ON sarplvup_amelia_events.id = sarplvup_amelia_events_periods.id 
    INNER JOIN `sarplvup_amelia_events_to_providers` ON sarplvup_amelia_events_periods.id = sarplvup_amelia_events_to_providers.eventId 
    INNER JOIN `sarplvup_amelia_users` ON sarplvup_amelia_events_to_providers.userId = sarplvup_amelia_users.id 
    WHERE sarplvup_amelia_customer_bookings.customerId = :customerId ORDER BY sarplvup_amelia_events_periods.periodStart ASC');

    $reqTabPres->bindValue(':customerId', $customerId, PDO::PARAM_INT);
    $reqTabPres->execute();
    $tabPres = $reqTabPres->fetchAll(PDO::FETCH_ASSOC);

    // trie les cours dans tableauPasse en fonction de la date de l'event et la date du jour
    for ($u = 0; $u < count($tabPres); $u++) {
        if ($tabPres[$u]['periodStart'] < $date) {
            array_push($tableauPasse, $tabPres[$u]);
        }
    }
    return $tableauPasse;
};