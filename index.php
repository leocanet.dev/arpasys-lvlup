<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./styles/style.css">
    <title>Connexion - Arpasys</title>
</head>

<?php include 'header.php'; ?>

<body>
    <main>
        <div class="container-full">
            <div class="wrapper-form">
                <div class="wrapper">
                    <?php
                    if (isset($_GET['login_err'])) {
                        $err = htmlspecialchars($_GET['login_err']);

                        switch ($err) {
                            case 'password':
                    ?>
                                <div class="alert">
                                    <p>
                                        <strong>Erreur :</strong> mot de passe incorrect
                                    </p>
                                </div>
                            <?php
                                break;

                            case 'email':
                            ?>
                                <div class="alert">
                                    <p>
                                        <strong>Erreur :</strong> email incorrect
                                    </p>
                                </div>
                            <?php
                                break;

                            case 'already':
                            ?>
                                <div class="alert">
                                    <p>
                                        <strong>Erreur :</strong> compte inexistant
                                    </p>
                                </div>
                            <?php
                                break;
                        }
                    }

                    if (isset($_GET['success'])) {
                        $success = htmlspecialchars($_GET['success']);

                        switch ($success) {
                            case 'success':
                            ?>
                                <div class="alert-ok">
                                    <p>
                                        <strong>Succès :</strong> inscription réussie !
                                    </p>
                                </div>
                            <?php
                                break;
                            case 'recover':
                            ?>
                                <div class="alert-ok">
                                    <p>
                                        <strong>Succès :</strong> l'email à bien été envoyé
                                    </p>
                                </div>
                            <?php
                                break;

                            case 'password_change':
                            ?>
                                <div class="alert-ok">
                                    <p>
                                        <strong>Succès :</strong> le mot de passe à été modifié
                                    </p>
                                </div>
                    <?php
                                break;
                        }
                    }
                    ?>

                    <form action="./controller/connexion.php" class="login-form" method="post">
                        <h1 class="text-center">Connexion</h1>
                        <div class="form__group">
                            <input type="email" name="email" class="form__field" required="required" autocomplete="off">
                            <label for="email" class="form__label">Email</label>
                            <span class="separator"> </span>
                        </div>
                        <div class="form__group">
                            <input type="password" name="password" class="form__field" required="required" autocomplete="off">
                            <label for="password" class="form__label">Mot de passe</label>
                            <span class="separator"> </span>
                        </div>
                        <div class="form-group action-wrapper">
                            <button type="submit" class="btn-submit">Connexion</button>
                            <a class="btn-submit" href="inscription.php">Inscription</a>
                        </div>
                    </form>

                    <div class="forgot-wrapper">
                        <p class="forgot-text">Avez-vous déjà effectué une commande sans création de compte ?</p>
                        <p class="forgot-text">Ne paniquez pas, pour avoir accès à votre compte cliquez sur le lien ci-dessous.</p>
                        <a href="mot-de-passe-oublie.php" style="color: #7350f2!important;">Mot de passe oublié</a>
                    </div>
                </div>
            </div>
        </div>
    </main>
</body>

</html>