// Get the modal
const modal = document.getElementById("landing-modal");
const passModal = document.getElementById("password-modal");
const tabPasseModal = document.getElementById("event-passe-modal");

// Get the button that opens the modal
const btn = document.getElementById("landing-btn");
const passBtn = document.getElementById("password-btn");
const boutonTabPasse = document.getElementById("tabPasse");

// Get the <span> element that closes the modal
const span = document.getElementsByClassName("close")[0];
const passSpan = document.getElementsByClassName("close")[1];
const tabPasseSpan = document.getElementsByClassName("close")[2];

// When the user clicks on the button, open the modal
btn.onclick = function () {
  modal.style.display = "block";
};

passBtn.onclick = function () {
  passModal.style.display = "block";
};

boutonTabPasse.onclick = function () {
  tabPasseModal.style.display = "block";
};

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
  modal.style.display = "none";
};

passSpan.onclick = function () {
  passModal.style.display = "none";
};

tabPasseSpan.onclick = function () {
  tabPasseModal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
  if (event.target == passModal) {
    passModal.style.display = "none";
  }
  if (event.target == tabPasseModal) {
    tabPasseModal.style.display = "none";
  }
};

// Systeme de pagination pour la version Desktop
const paginationNumbers = document.getElementById("pagination-numbers");
const paginatedList = document.getElementById("paginated-list");
const listItems = paginatedList.getElementsByClassName("t-row");
const nextButton = document.getElementById("next-button");
const prevButton = document.getElementById("prev-button");
const paginationLimit = 4;
const pageCount = Math.ceil(listItems.length / paginationLimit);
let currentPage = 1;

const disableButton = (button) => {
  button.classList.add("disabled");
  button.setAttribute("disabled", true);
};

const enableButton = (button) => {
  button.classList.remove("disabled");
  button.removeAttribute("disabled");
};
// Desactive les boutons "<",">" si la page = 1 et si l'user et sur la dernière page
const handlePageButtonsStatus = () => {
  if (currentPage === 1) {
    disableButton(prevButton);
  } else {
    enableButton(prevButton);
  }

  if (pageCount === currentPage) {
    disableButton(nextButton);
  } else {
    enableButton(nextButton);
  }
};
// Ajoute la classe active sur le bouton de la page correspondante et enleve la classe active si le bouton ne correspondant pas à la page actuelle
const handleActivePageNumber = () => {
  document.querySelectorAll(".pagination-number").forEach((button) => {
    button.classList.remove("active");
    const pageIndex = Number(button.getAttribute("page-index"));
    if (pageIndex == currentPage) {
      button.classList.add("active");
    }
  });
};
// Créer un bouton avec les attributs correspondant à la page
const appendPageNumber = (index) => {
  const pageNumber = document.createElement("button");
  pageNumber.className = "pagination-number";
  pageNumber.innerHTML = index;
  pageNumber.setAttribute("page-index", index);
  pageNumber.setAttribute("aria-label", "Page " + index);

  paginationNumbers.appendChild(pageNumber);
};
// Créer le nombre de boutons correspondant aux nombres de pages
const getPaginationNumbers = () => {
  for (let i = 1; i <= pageCount; i++) {
    appendPageNumber(i);
  }
};
// Effectue des changements à l'aide des fonctions précedentes en fonction du numero de page
const setCurrentPage = (pageNum) => {
  currentPage = pageNum;

  handleActivePageNumber();
  handlePageButtonsStatus();

  const prevRange = (pageNum - 1) * paginationLimit;
  const currRange = pageNum * paginationLimit;

  // Utilisaton de Array.from pour formatter un Array-like object (listItems) crée par getElementsByClassName
  Array.from(listItems).forEach((item, index) => {
    item.classList.add("hidden");
    if (index >= prevRange && index < currRange) {
      item.classList.remove("hidden");
    }
  });
};
// Execute des le chargement de la page la pagination est l'initalise à la page 1
window.addEventListener("load", () => {
  getPaginationNumbers();
  setCurrentPage(1);
  // Au clique sur prevButton page - 1
  prevButton.addEventListener("click", () => {
    setCurrentPage(currentPage - 1);
  });
  // Au clique sur nextButton page + 1
  nextButton.addEventListener("click", () => {
    setCurrentPage(currentPage + 1);
  });
  // Au clique sur les boutons de pagination définit la page avec les attributs correspondants
  document.querySelectorAll(".pagination-number").forEach((button) => {
    const pageIndex = Number(button.getAttribute("page-index"));

    if (pageIndex) {
      button.addEventListener("click", () => {
        setCurrentPage(pageIndex);
      });
    }
  });
});

// Systeme de pagination pour la version Mobile
const paginationNumbersList = document.getElementById(
  "pagination-numbers-list"
);
const paginatedListList = document.getElementById("paginated-list-list");
const listItemsList = paginatedListList.getElementsByClassName("page-list");
const nextButtonList = document.getElementById("next-button-list");
const prevButtonList = document.getElementById("prev-button-list");
const paginationLimitList = 4;
const pageCountList = Math.ceil(listItemsList.length / paginationLimitList);
let currentPageList = 1;

const disableButtonList = (button) => {
  button.classList.add("disabled");
  button.setAttribute("disabled", true);
};

const enableButtonList = (button) => {
  button.classList.remove("disabled");
  button.removeAttribute("disabled");
};
// Desactive les boutons "<",">" si la page = 1 et si l'user et sur la dernière page
const handlePageButtonsStatusList = () => {
  if (currentPageList === 1) {
    disableButtonList(prevButtonList);
  } else {
    enableButtonList(prevButtonList);
  }

  if (pageCountList === currentPageList) {
    disableButtonList(nextButtonList);
  } else {
    enableButtonList(nextButtonList);
  }
};
// Ajoute la classe active sur le bouton de la page correspondante et enleve la classe active si le bouton ne correspondant pas à la page actuelle
const handleActivePageNumberList = () => {
  document.querySelectorAll(".pagination-number").forEach((button) => {
    button.classList.remove("active");
    const pageIndexList = Number(button.getAttribute("page-index"));
    if (pageIndexList == currentPageList) {
      button.classList.add("active");
    }
  });
};
// Créer un bouton avec les attributs correspondant à la page
const appendPageNumberList = (index) => {
  const pageNumberList = document.createElement("button");
  pageNumberList.className = "pagination-number";
  pageNumberList.innerHTML = index;
  pageNumberList.setAttribute("page-index", index);
  pageNumberList.setAttribute("aria-label", "Page " + index);

  paginationNumbersList.appendChild(pageNumberList);
};
// Créer le nombre de boutons correspondant aux nombres de pages
const getPaginationNumbersList = () => {
  for (let i = 1; i <= pageCountList; i++) {
    appendPageNumberList(i);
  }
};
// Effectue des changements à l'aide des fonctions précedentes en fonction du numero de page
const setCurrentPageList = (pageNum) => {
  currentPageList = pageNum;

  handleActivePageNumberList();
  handlePageButtonsStatusList();

  const prevRangeList = (pageNum - 1) * paginationLimitList;
  const currRangeList = pageNum * paginationLimitList;

  // Utilisaton de Array.from pour formatter un Array-like object (listItems) crée par getElementsByClassName
  Array.from(listItemsList).forEach((item, index) => {
    item.classList.add("hidden");
    if (index >= prevRangeList && index < currRangeList) {
      item.classList.remove("hidden");
    }
  });
};
// Execute des le chargement de la page la pagination est l'initalise à la page 1
window.addEventListener("load", () => {
  getPaginationNumbersList();
  setCurrentPageList(1);
  // Au clique sur prevButton page - 1
  prevButtonList.addEventListener("click", () => {
    setCurrentPageList(currentPageList - 1);
  });
  // Au clique sur nextButton page + 1
  nextButtonList.addEventListener("click", () => {
    setCurrentPageList(currentPageList + 1);
  });
  // Au clique sur les boutons de pagination définit la page avec les attributs correspondants
  document.querySelectorAll(".pagination-number").forEach((button) => {
    const pageIndexList = Number(button.getAttribute("page-index"));

    if (pageIndexList) {
      button.addEventListener("click", () => {
        setCurrentPageList(pageIndexList);
      });
    }
  });
});
