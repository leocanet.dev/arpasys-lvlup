<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit4f37eb54af36d04a1015d1a6c92a5f9d
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PHPMailer\\PHPMailer\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PHPMailer\\PHPMailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpmailer/phpmailer/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit4f37eb54af36d04a1015d1a6c92a5f9d::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit4f37eb54af36d04a1015d1a6c92a5f9d::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit4f37eb54af36d04a1015d1a6c92a5f9d::$classMap;

        }, null, ClassLoader::class);
    }
}
