<header>
    <img class="logo" src="./assets/images/arpasyslvlup.webp" alt="Arpasys logo">
    <div class="fullnav">
        <nav class="navigation">
            <ul class="nav">
                <li><a class="nav-link" href="https://www.arpasys.com/">ACCUEIL</a></li>
                <li><a class="nav-link" href="https://www.arpasys.com/ss2i/">ARPASYS</a></li>
                <li><a class="nav-link" href="https://www.arpasys.com/store/">ARPASYS STORE</a></li>
                <li><a class="nav-link" href="https://www.arpasys.com/construction.php">ARPASYS STUDIO</a></li>
                <li><a class="nav-link" href="https://test.arpaview.fr/SITE-ARPASYS-LVLUP/">ARPASYS LEVEL UP</a></li>

            </ul>
        </nav>
    </div>
    <div class="minnav">
        <div id="mySidenav" class="sidenav burger">
            <a id="closeBtn" class="closeBtn">&times;</a>
            <ul>
                <li><a class="nav-link" href="https://www.arpasys.com/">Accueil</a></li>
                <li><a class="nav-link" href="https://www.arpasys.com/ss2i/">Arpasys</a></li>
                <li><a class="nav-link" href="https://www.arpasys.com/store/">Arpasys Store</a></li>
                <li><a class="nav-link" href="https://www.arpasys.com/construction.php">Arpasys studio</a></li>
                <li><a class="nav-link" href="https://test.arpaview.fr/SITE-ARPASYS-LVLUP/">Arpasys Level Up</a></li>
            </ul>
        </div>

        <a id="openBtn">
            <span class="burger-icon">
                <span></span>
                <span></span>
                <span></span>
            </span>
        </a>
    </div>
</header>
<script>
    var sidenav = document.getElementById("mySidenav");
    var openBtn = document.getElementById("openBtn");
    var closeBtn = document.getElementById("closeBtn");

    openBtn.onclick = openNav;
    closeBtn.onclick = closeNav;

    /* Set the width of the side navigation to 250px */
    function openNav() {
        sidenav.classList.add("active");
    }

    /* Set the width of the side navigation to 0 */
    function closeNav() {
        sidenav.classList.remove("active");
    }
</script>