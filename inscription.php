<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./styles/style.css">
    <title>Inscription - Arpasys</title>
</head>

<?php include 'header.php'; ?>

<body>
    <div class="container-full">
        <div class="wrapper-form">
            <div class="wrapper">
                <?php
                if (isset($_GET['reg_err'])) {
                    $err = htmlspecialchars($_GET['reg_err']);

                    switch ($err) {
                        case 'password':
                ?>
                            <div class="alert">
                                <p>
                                    <strong>Erreur</strong> mot de passe différent
                                </p>
                            </div>
                        <?php
                            break;

                        case 'email':
                        ?>
                            <div class="alert">
                                <p>
                                    <strong>Erreur</strong> email non valide
                                </p>
                            </div>
                        <?php
                            break;

                        case 'email_length':
                        ?>
                            <div class="alert">
                                <p>
                                    <strong>Erreur</strong> nombre maximum de caractères : Email
                                </p>
                            </div>
                        <?php
                            break;

                        case 'prenom_length':
                        ?>
                            <div class="alert">
                                <p>
                                    <strong>Erreur</strong> nombre maximum de caractères : Prenom
                                </p>
                            </div>
                        <?php
                            break;

                        case 'nom_length':
                        ?>
                            <div class="alert">
                                <p>
                                    <strong>Erreur</strong> nombre maximum de caractères : Nom
                                </p>
                            </div>
                        <?php
                            break;

                        case 'already':
                        ?>
                            <div class="alert">
                                <p>
                                    <strong>Erreur</strong> compte deja existant
                                </p>
                            </div>
                        <?php
                            break;
                        case 'city':
                        ?>
                            <div class="alert">
                                <p>
                                    <strong>Erreur</strong> nombre maximum de caractères : city
                                </p>
                            </div>
                        <?php
                            break;
                        case 'zipcode':
                        ?>
                            <div class="alert">
                                <p>
                                    <strong>Erreur</strong> nombre maximum de caractères : Code postal
                                </p>
                            </div>
                        <?php
                            break;
                        case 'rue':
                        ?>
                            <div class="alert">
                                <p>
                                    <strong>Erreur</strong> nombre maximum de caractères : Rue
                                </p>
                            </div>
                <?php
                            break;
                    }
                }



                ?>

                <form action="./controller/inscription_controller.php" class="login-form" method="post">
                    <h1 class="text-center heading-mb">Inscription</h1>
                    <div class="box-wrapper">
                        <div class="box-1">
                            <div class="form__group">
                                <input type="text" name="prenom" class="form__field" required="required" autocomplete="off">
                                <label for="prenom" class="form__label">Prenom <abbr>*</abbr></label>
                                <span class="separator"> </span>
                            </div>
                            <div class="form__group">
                                <input type="text" name="nom" class="form__field" required="required" autocomplete="off">
                                <label for="nom" class="form__label">Nom <abbr>*</abbr></label>
                                <span class="separator"> </span>
                            </div>
                            <div class="form__group">
                                <input type="text" name="naissance" class="form__field" placeholder="Date de naissance *" onfocus="(this.type='date')" required="required" autocomplete="off">
                                <label for="naissance" class="form__label">Date de naissance <abbr>*</abbr></label>
                                <span class="separator"> </span>
                            </div>
                            <div class="form__group">
                                <input type="email" name="email" class="form__field" required="required" autocomplete="off">
                                <label for="email" class="form__label">Email <abbr>*</abbr></label>
                                <span class="separator"> </span>
                            </div>
                            <div class="form__group">
                                <input type="tel" name="phone" class="form__field" required="required" autocomplete="off" min="10" max="10" pattern="[0-9]{10}">
                                <label for="phone" class="form__label">Téléphone <abbr>*</abbr></label>
                                <span class="separator"> </span>
                            </div>
                            <div class="form__group">
                                <input type="text" name="rue" class="form__field" required="required" autocomplete="off">
                                <label for="rue" class="form__label">Adresse <abbr>*</abbr></label>
                                <span class="separator"> </span>
                            </div>
                        </div>
                        <div class="box-2">
                            <div class="form__group">
                                <input type="tel" name="zipcode" class="form__field" required="required" autocomplete="off" min="4" max="5" pattern="[0-9]{4,5}">
                                <label for="zipcode" class="form__label">Code postal <abbr>*</abbr></label>
                                <span class="separator"> </span>
                            </div>
                            <div class="form__group">
                                <input type="text" name="city" class="form__field" required="required" autocomplete="off">
                                <label for="city" class="form__label">Ville <abbr>*</abbr></label>
                                <span class="separator"> </span>
                            </div>
                            <div class="form__group">
                                <input type="password" name="password" class="form__field" required="required" autocomplete="off">
                                <label for="passsord" class="form__label">Mot de passe <abbr>*</abbr></label>
                                <span class="separator"> </span>
                            </div>
                            <div class="form__group">
                                <input type="password" name="password_retype" class="form__field" required="required" autocomplete="off">
                                <label for="password_retype" class="form__label">Confirmer <abbr>*</abbr></label>
                                <span class="separator"> </span>
                            </div>
                            <div class="form__group">
                                <input type="text" name="societe" class="form__field" autocomplete="off">
                                <label for="societe" class="form__label">Société</label>
                                <span class="separator"> </span>
                            </div>
                            <div class="form__group">
                                <input type="tel" name="tva" class="form__field" autocomplete="off" min="10" max="15" pattern="[0-9]{10,15}">
                                <label for="tva" class="form__label">N° TVA</label>
                                <span class="separator"> </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group action-wrapper">
                        <a class="btn-submit" href="index.php">Connexion</a>
                        <button type="submit" class="btn-submit">Inscription</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</body>

</html>